package mifareplus.jsonparsing;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.HashMap;

import mifareplus.Converter;

/**
 * Created by emercado on 9/18/15.
 */
public class DocumentStructure {

    public static  HashMap<String, String> tagData= new HashMap<String, String>();

    public static JSONObject jsonDocumentType ;
    public static byte[] keysDocumentType;

    public static JSONObject jsonBirthCert;
    public static JSONArray memDistBirthCert;
    public static HashMap<Integer, byte[]> keysBirthCert;

    public static JSONObject jsonDeathCert;
    public static JSONArray memDistDeathCert;
    public static HashMap<Integer, byte[]>  keysDeathCert;

    public static void createDocumentStructures(InputStream documentType, InputStream birthCert){
        try {
            jsonDocumentType = new JSONObject(readJsonFile(documentType));
            keysDocumentType= Converter.hexStringToByteArray(jsonDocumentType.getString("key"));

            jsonBirthCert = new JSONObject(readJsonFile(birthCert));
            memDistBirthCert = jsonBirthCert.getJSONArray("memoryDistribution");
            keysBirthCert=convertKeysToMap(jsonBirthCert.getJSONArray("keysPerSector"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private static String readJsonFile(InputStream is) {
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return writer.toString();
    }

    private static HashMap<Integer, byte[]>  convertKeysToMap(JSONArray keysPerSec){
        HashMap<Integer, byte[]> keys= new HashMap<Integer, byte[]>();
        try {
            for (int i = 0; i < keysPerSec.length(); i++) {
                JSONObject jo_inside = keysPerSec.getJSONObject(i);
                int sector = jo_inside.getInt("sectorNumber");
                String sector_key = jo_inside.getString("key");
                keys.put(sector,Converter.hexStringToByteArray(sector_key));
            }
        } catch (Exception e) {
            //e.printStackTrace();
            e.printStackTrace();
        }
        return keys;
    }
    public static void cleanData(){
        tagData.clear();
    }
}
