package mifareplus;

import java.text.SimpleDateFormat;

/**
 * Created by emercado on 9/3/15.
 */
public class Converter {
    private static final byte[] HEX_CHAR_TABLE = {(byte) '0', (byte) '1',
            (byte) '2', (byte) '3', (byte) '4', (byte) '5', (byte) '6',
            (byte) '7', (byte) '8', (byte) '9', (byte) 'A', (byte) 'B',
            (byte) 'C', (byte) 'D', (byte) 'E', (byte) 'F'};

    public static String getHexString(byte[] raw, int len) {
        byte[] hex = new byte[2 * len];
        int index = 0;
        int pos = 0;


        for (byte b : raw) {
            if (pos >= len)
                break;

            pos++;
            int v = b & 0xFF;
            hex[index++] = HEX_CHAR_TABLE[v >>> 4];
            hex[index++] = HEX_CHAR_TABLE[v & 0xF];
        }
        return new String(hex);
    }

    public static String getHexString(byte[] raw, int len, int startByte, int size) {
        byte[] hex = new byte[2 * len];
        int index = 0;
        int pos = 0;
        int count = 0;


        for (byte b : raw) {
            if (pos >= len)
                break;

            if (pos >= startByte && count <= size - 1) {

                int v = b & 0xFF;
                hex[index++] = HEX_CHAR_TABLE[v >>> 4];
                hex[index++] = HEX_CHAR_TABLE[v & 0xF];
                count++;
            }
            pos++;
        }
        return new String(hex);
    }

    public static String hexToAscii(String s) {
        String cleanStr = s.replace("\u0000", "");
        int n = cleanStr.length();
        StringBuilder sb = new StringBuilder(n / 2);
        for (int i = 0; i < n; i += 2) {
            char a = cleanStr.charAt(i);
            char b = cleanStr.charAt(i + 1);
            sb.append((char) ((hexToInt(a) << 4) | hexToInt(b)));
        }
        return sb.toString();
    }

    private static int hexToInt(char ch) {
        if ('a' <= ch && ch <= 'f') {
            return ch - 'a' + 10;
        }
        if ('A' <= ch && ch <= 'F') {
            return ch - 'A' + 10;
        }
        if ('0' <= ch && ch <= '9') {
            return ch - '0';
        }
        throw new IllegalArgumentException(String.valueOf(ch));
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public static String bytesToHex(byte[] bytes) {
        char[] hexArray = "0123456789ABCDEF".toCharArray();

        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static String convertDateFormat(String inputDate) {
        SimpleDateFormat fromUser = new SimpleDateFormat("ddMMyyyy");
        SimpleDateFormat myFormat = new SimpleDateFormat("dd/MM/yyyy");

        try {

            String reformattedStr = myFormat.format(fromUser.parse(inputDate));
            return reformattedStr;
        } catch (Exception e) {
            return inputDate;
        }
    }

    public static String convertGender(String input) {
        if (input.equals("M"))
            return "Masculino";
        else if (input.equals("F"))
            return "Femenino";
        else
            return input;

    }
}
