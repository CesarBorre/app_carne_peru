package com.neo.peru_carne.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.neo.peru_carne.R;
import com.neo.peru_carne.model.Medical_Contradictions;

/**
 * Created by cesar on 10/11/15.
 */
public class Medical_Contradictions_Adapter extends ArrayAdapter<Medical_Contradictions> {
    Context c;
    TextView strMedicalContradiction;
    ImageView icon;

    public Medical_Contradictions_Adapter(Context context) {
        super(context, R.layout.medical_contradictions_row);
        this.c = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        LayoutInflater layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = layoutInflater.inflate(R.layout.medical_contradictions_row, null);

        strMedicalContradiction = (TextView) v.findViewById(R.id.medical_Contradiction_row);
        icon = (ImageView) v.findViewById(R.id.icon_Medical_Contradiction_row);

        strMedicalContradiction.setText(getItem(position).getStrMedicalContradiction());
        icon.setImageResource(getItem(position).getIcon());

        return v;
    }
}
