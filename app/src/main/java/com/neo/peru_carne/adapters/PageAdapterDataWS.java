package com.neo.peru_carne.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.neo.peru_carne.listeners.UpdateableFragmentListener;
import com.neo.peru_carne.subfragments.BackData;
import com.neo.peru_carne.subfragments.FrontData;

/**
 * Created by cesar on 4/11/15.
 */
public class PageAdapterDataWS extends FragmentStatePagerAdapter {
    public PageAdapterDataWS(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment frag = null;
        if ( position == 0) {
            frag = new FrontData();
        } else if(position == 1) {
            frag = new BackData();
        }
        return frag;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public int getItemPosition(Object object) {
        if (object instanceof UpdateableFragmentListener) {
            ((UpdateableFragmentListener) object).onUpdated();
        }
        //don't return POSITION_NONE, avoid fragment recreation.
        return super.getItemPosition(object);
    }
}
