package com.neo.peru_carne;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.neo.peru_carne.model.CarnePeData;
import com.neo.peru_carne.subfragments.FragParentData;

/**
 * Created by cesar on 5/11/15.
 */
public class DataActivity extends AppCompatActivity {

    private FragParentData fragParentData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_ws);
        showParentFragment();
    }

    private void showParentFragment(){
        fragParentData = new FragParentData();
        makeFragmentTransaction(fragParentData);
    }

    private void makeFragmentTransaction(Fragment fragment) {
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
        }
    }
}
