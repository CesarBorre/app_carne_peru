package com.neo.peru_carne.model;

import java.io.Serializable;

/**
 * Created by cesar on 5/11/15.
 */
public class CarnePeData implements Serializable {


    private String intNumeroIdentificacion;
    //                String dtFechaVigenciaIdentificacion = carneData.getString("dtFechaVigenciaIdentificacion");
    private String strTipoSangre;
    private String strTelefonoEmergencia;
    private String strNombre;
    private String strApePaterno;
    private String strApeMaterno;
    //            String strLugarNac = carneData.getString("strLugarNac");
    private String strNacionalidad;
    private String strInstituto;
    private int intCodigo;
    private String strCarrera;
    private String strGradoEstudios;
    private double dLat_Ubicacion;
    private double dLng_Ubicacion;
    private String strContradiccionMedica_1;
    private String strContradiccionMedica_2;
    private String strContradiccionMedica_3;
    private String strContradiccionMedica_4;
    private String strContradiccionMedica_5;
    private String strContradiccionMedica_6;
    private int id;
    private byte[] img;

    public CarnePeData() {

    }

    public CarnePeData(String strNombre) {
        this.strNombre = strNombre;
    }

    public CarnePeData(
            String intNumeroIdentificacion,
            String strTipoSangre,
            String strTelefonoEmergencia,
            String strNombre,
            String strApePaterno,
            String strApeMaterno,
            String strNacionalidad,
            String strInstituto,
            int intCodigo,
            String strCarrera,
            String strGradoEstudios,
            double dLat_Ubicacion,
            double dLng_Ubicacion,
            String strContradiccionMedica_1,
            String strContradiccionMedica_2,
            String strContradiccionMedica_3,
            String strContradiccionMedica_4,
            String strContradiccionMedica_5,
            String strContradiccionMedica_6,
            int id,
            byte[] img) {
        this.intNumeroIdentificacion = intNumeroIdentificacion;
        this.strTipoSangre = strTipoSangre;
        this.strTelefonoEmergencia = strTelefonoEmergencia;
        this.strNombre = strNombre;
        this.strApePaterno = strApePaterno;
        this.strApeMaterno = strApeMaterno;
        this.strNacionalidad = strNacionalidad;
        this.strInstituto = strInstituto;
        this.intCodigo = intCodigo;
        this.strCarrera = strCarrera;
        this.strGradoEstudios = strGradoEstudios;
        this.dLat_Ubicacion = dLat_Ubicacion;
        this.dLng_Ubicacion = dLng_Ubicacion;
        this.strContradiccionMedica_1 = strContradiccionMedica_1;
        this.strContradiccionMedica_2 = strContradiccionMedica_2;
        this.strContradiccionMedica_3 = strContradiccionMedica_3;
        this.strContradiccionMedica_4 = strContradiccionMedica_4;
        this.strContradiccionMedica_5 = strContradiccionMedica_5;
        this.strContradiccionMedica_6 = strContradiccionMedica_6;
        this.id = id;
        this.img = img;
    }


    public String getIntNumeroIdentificacion() {
        return intNumeroIdentificacion;
    }

    public void setIntNumeroIdentificacion(String intNumeroIdentificacion) {
        this.intNumeroIdentificacion = intNumeroIdentificacion;
    }

    public String getStrTipoSangre() {
        return strTipoSangre;
    }

    public void setStrTipoSangre(String strTipoSangre) {
        this.strTipoSangre = strTipoSangre;
    }

    public String getStrTelefonoEmergencia() {
        return strTelefonoEmergencia;
    }

    public void setStrTelefonoEmergencia(String strTelefonoEmergencia) {
        this.strTelefonoEmergencia = strTelefonoEmergencia;
    }

    public String getStrNombre() {
        return strNombre;
    }

    public void setStrNombre(String strNombre) {
        this.strNombre = strNombre;
    }

    public String getStrApePaterno() {
        return strApePaterno;
    }

    public void setStrApePaterno(String strApePaterno) {
        this.strApePaterno = strApePaterno;
    }

    public String getStrApeMaterno() {
        return strApeMaterno;
    }

    public void setStrApeMaterno(String strApeMaterno) {
        this.strApeMaterno = strApeMaterno;
    }

    public String getStrNacionalidad() {
        return strNacionalidad;
    }

    public void setStrNacionalidad(String strNacionalidad) {
        this.strNacionalidad = strNacionalidad;
    }

    public String getStrInstituto() {
        return strInstituto;
    }

    public void setStrInstituto(String strInstituto) {
        this.strInstituto = strInstituto;
    }

    public int getIntCodigo() {
        return intCodigo;
    }

    public void setIntCodigo(int intCodigo) {
        this.intCodigo = intCodigo;
    }

    public String getStrCarrera() {
        return strCarrera;
    }

    public void setStrCarrera(String strCarrera) {
        this.strCarrera = strCarrera;
    }

    public String getStrGradoEstudios() {
        return strGradoEstudios;
    }

    public void setStrGradoEstudios(String strGradoEstudios) {
        this.strGradoEstudios = strGradoEstudios;
    }

    public double getdLat_Ubicacion() {
        return dLat_Ubicacion;
    }

    public void setdLat_Ubicacion(double dLat_Ubicacion) {
        this.dLat_Ubicacion = dLat_Ubicacion;
    }

    public double getdLng_Ubicacion() {
        return dLng_Ubicacion;
    }

    public void setdLng_Ubicacion(double dLng_Ubicacion) {
        this.dLng_Ubicacion = dLng_Ubicacion;
    }

    public String getStrContradiccionMedica_1() {
        return strContradiccionMedica_1;
    }

    public void setStrContradiccionMedica_1(String strContradiccionMedica_1) {
        this.strContradiccionMedica_1 = strContradiccionMedica_1;
    }

    public String getStrContradiccionMedica_2() {
        return strContradiccionMedica_2;
    }

    public void setStrContradiccionMedica_2(String strContradiccionMedica_2) {
        this.strContradiccionMedica_2 = strContradiccionMedica_2;
    }

    public String getStrContradiccionMedica_3() {
        return strContradiccionMedica_3;
    }

    public void setStrContradiccionMedica_3(String strContradiccionMedica_3) {
        this.strContradiccionMedica_3 = strContradiccionMedica_3;
    }

    public String getStrContradiccionMedica_4() {
        return strContradiccionMedica_4;
    }

    public void setStrContradiccionMedica_4(String strContradiccionMedica_4) {
        this.strContradiccionMedica_4 = strContradiccionMedica_4;
    }

    public String getStrContradiccionMedica_5() {
        return strContradiccionMedica_5;
    }

    public void setStrContradiccionMedica_5(String strContradiccionMedica_5) {
        this.strContradiccionMedica_5 = strContradiccionMedica_5;
    }

    public String getStrContradiccionMedica_6() {
        return strContradiccionMedica_6;
    }

    public void setStrContradiccionMedica_6(String strContradiccionMedica_6) {
        this.strContradiccionMedica_6 = strContradiccionMedica_6;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public byte[] getImg() {
        return img;
    }

    public void setImg(byte[] img) {
        this.img = img;
    }
}
