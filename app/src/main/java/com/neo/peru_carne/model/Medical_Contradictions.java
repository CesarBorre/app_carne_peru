package com.neo.peru_carne.model;

import android.graphics.Bitmap;

/**
 * Created by cesar on 10/11/15.
 */
public class Medical_Contradictions {

    private String strMedicalContradiction;
    private int icon;

    public Medical_Contradictions() {

    }

    public Medical_Contradictions(String strMedicalContradiction, int icon) {
        this.setStrMedicalContradiction(strMedicalContradiction);
        this.setIcon(icon);
    }


    public String getStrMedicalContradiction() {
        return strMedicalContradiction;
    }

    public void setStrMedicalContradiction(String strMedicalContradiction) {
        this.strMedicalContradiction = strMedicalContradiction;
    }


    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
