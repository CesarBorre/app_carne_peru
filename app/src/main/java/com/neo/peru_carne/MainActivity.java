package com.neo.peru_carne;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.MifareClassic;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.Toast;

import com.neo.peru_carne.utils.EbreederSettings;
import com.nxp.nfcliblite.Interface.NxpNfcLibLite;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import mifareplus.Converter;
import mifareplus.jsonparsing.DocumentStructure;

public class MainActivity extends AppCompatActivity {

    static final String TAG = MainActivity.class.getSimpleName();

    private Toolbar mToolbar;
    private NxpNfcLibLite libInstance = null;
    private static Tag tagFromIntent;
    private static MifareClassic mfc;

    int errorType = 0;
    int documentType;
    String errorMsg = "";
    boolean error = false;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher_neo);

        loadWebView();

        libInstance = NxpNfcLibLite.getInstance();
        libInstance.registerActivity(this);

        createProgressDIalog();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            EbreederSettings.showSettingsDialog(getApplicationContext(), MainActivity.this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void loadWebView() {
        WebView wv = (WebView) findViewById(R.id.webView);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.loadUrl("file:///android_asset/tap.html");
    }

    private void createProgressDIalog() {
        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMax(100);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Leyendo Carné...espere");
    }

    private byte firstBlockInSector(int sector) {
        if (sector < 32) {
            return (byte) ((sector * 4) & 0xff);
        } else {
            return (byte) ((32 * 4 + ((sector - 32) * 16)) & 0xff);
        }
    }

    @Override
    protected void onNewIntent(final Intent intent) {
        try {
            tagFromIntent = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            mfc = MifareClassic.get(tagFromIntent);
            if (mfc == null) {
                Context context = getApplicationContext();
                CharSequence text = "Problemas con el lector";
                int durationMsg = Toast.LENGTH_LONG;

                Toast toast = Toast.makeText(context, text, durationMsg);
                toast.show();
            } else {

                LongOperation op = new LongOperation();
                op.execute();
            }
        } catch (Exception ex) {
            Log.e(TAG, "Error: " + ex.getMessage());
        }
    }

    private class LongOperation extends AsyncTask<Void, Integer, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            documentType = 0;
            error = false;
            errorType = 0;
            DocumentStructure.cleanData();
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                mfc.connect();
                mfc.setTimeout(5000);
                documentType = Integer.parseInt(getDocumentType(DocumentStructure.jsonDocumentType, DocumentStructure.keysDocumentType, mfc));

                Log.i(TAG, "Document type: " + documentType + "-----");
                if (documentType == 1) {
                    readTagData(DocumentStructure.jsonBirthCert, DocumentStructure.memDistBirthCert, DocumentStructure.keysBirthCert, mfc);
//                    int count = 0;
//                    long total = 0;
//                    while (count == lenghtFile) {
//                        total += count;
//                        publishProgress((int)((total*100)/lenghtFile));
//                    }
                }
                Vibrator vib = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                long[] duration = {50, 100, 200, 300};
                vib.vibrate(duration, -1);
            } catch (IOException e) {
                e.printStackTrace();
                error = true;
                errorMsg = "Error while reading, try again.";
            } catch (JSONException e) {
                e.printStackTrace();
                error = true;
                errorType = 1;
                errorMsg = "INVALID TAG.";
            }
            return error;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
//            progressDialog.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result) {
                if (errorType == 1) {
                    Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_SHORT).show();
                }
            } else {
                if (documentType == 1) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        // Call android transitions
                    } else {
                        Intent i = new Intent(getApplicationContext(), TagActivity.class);
                        startActivity(i);
//                        Intent i = new Intent(getApplicationContext(), TagActivity.class);
//                        ActivityTransitionLauncher.with(MainActivity.this).from(v).launch(i);
                    }
                }
            }
        }
    }

    private String getDocumentType(JSONObject obj, byte[] keys, MifareClassic mfc) throws IOException, JSONException {
        boolean auth = false;
        byte[] data;
        String result = "";
        auth = mfc.authenticateSectorWithKeyA(obj.getInt("sector"), keys);
        if (auth) {
            byte addr = (byte) ((firstBlockInSector(obj.getInt("sector")) + obj.getInt("block")) & 0xff);
            data = mfc.readBlock(addr);
            Log.i(TAG, data.toString());
            result = Converter.hexToAscii(Converter.getHexString(data, data.length, obj.getInt("startByte"), obj.getInt("size")));
        } else {
            error = true;
            errorMsg = "Security warning, one or more key are invalid.";
        }
        return result;
    }

    private void readTagData(JSONObject obj, JSONArray memoryDistribution, HashMap<Integer, byte[]> keys, MifareClassic mfc) {
        try {
            for (int i = 0; i < memoryDistribution.length(); i++) {
                JSONObject jo_inside = memoryDistribution.getJSONObject(i);
                String name_value = jo_inside.getString("name");
                int sector_value = jo_inside.getInt("sector");
                int block_value = jo_inside.getInt("block");
                int startByte_value = jo_inside.getInt("startByte");
                int size_value = jo_inside.getInt("size");
                String resultTmp = getData(sector_value, block_value, startByte_value, size_value, keys, mfc);
                DocumentStructure.tagData.put(name_value, resultTmp);
                //result=result+name_value+": "+resultTmp;
            }
        } catch (Exception e) {
            //e.printStackTrace();
            Log.i(TAG, e.getMessage());
            errorMsg = e.getMessage();
            error = true;
        }
    }

    private String getData(int sector, int block, int startByte, int size, HashMap<Integer, byte[]> keys, MifareClassic mfc) throws IOException {
        float chunkSize = (float) size / 16;
        int numberOfBlocks = (int) Math.ceil(chunkSize);
        int totalBlocks = numberOfBlocks;
        int sectorIndex = sector;
        int blockIndex = block;
        boolean auth = false;
        byte[] data;
        String resultStrGlobal = "";
        // Log.i(TAG, "---------- Sector:" + sector+ " Block:" + block+" Byte:" + startByte+ " Size:"+size+ " Blocks:"+totalBlocks+" -------");
        byte[] keySector;
        if (keys.get(sectorIndex) != null)
            keySector = keys.get(sectorIndex);
        else
            keySector = MifareClassic.KEY_DEFAULT;
        for (int i = 0; i <= totalBlocks - 1; i++) {
            // Log.i(TAG, "Trying to authenticate sector:" + sectorIndex + " with the key:" + Converter.bytesToHex(keySector));
            auth = mfc.authenticateSectorWithKeyA(sectorIndex, keySector);
            if (auth) {
                byte addr = (byte) ((firstBlockInSector(sectorIndex) + blockIndex) & 0xff);
                data = mfc.readBlock(addr);
                String resultStr = Converter.hexToAscii(Converter.getHexString(data, data.length, startByte, size));
                resultStrGlobal = resultStrGlobal + resultStr;
                if (size >= 16)
                    size = size - 16;
            } else {
                error = true;
                errorMsg = "Security warning, one or more key are invalid.";
                break;
            }

            if (blockIndex >= 2) {
                sectorIndex = sectorIndex + 1;
                blockIndex = 0;
            } else {
                blockIndex = blockIndex + 1;
            }
        }
        // Log.i(TAG, "========================================");
        resultStrGlobal = resultStrGlobal.trim();
        return resultStrGlobal;
    }

    @Override
    protected void onPause() {
        libInstance.stopForeGroundDispatch();
        super.onPause();
    }

    @Override
    protected void onResume() {
        libInstance.startForeGroundDispatch();
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }
}
