package com.neo.peru_carne;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.neo.peru_carne.utils.Constants_Settings;
import com.neo.peru_carne.utils.Preferences;

import java.io.InputStream;

import mifareplus.jsonparsing.DocumentStructure;

public class SplashActivity extends AppCompatActivity {

    private CountDownTimer mCountDownTimer;
    Preferences preferences;
    Intent intent = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!isTaskRoot()) {
            final Intent intent = getIntent();
            if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && Intent.ACTION_MAIN.equals(intent.getAction())) {
                Log.w("carne peru", "Main Activity is not the root.  Finishing Main Activity instead of launching.");
                finish();
                return;
            }
        }
        setContentView(R.layout.activity_splash);
        InputStream documentType = getResources().openRawResource(R.raw.document_type_map);
        InputStream birthCert = getResources().openRawResource(R.raw.birth_certificate_map);

        DocumentStructure.createDocumentStructures(documentType, birthCert);
        DocumentStructure.cleanData();

        preferences = new Preferences(Constants_Settings.SHARED_PREF_NAME, getApplicationContext());

        if(!preferences.isExist(Constants_Settings.KEY_HOST_IP)) {
            preferences.Write_String(Constants_Settings.KEY_URL, Constants_Settings.SHARED_PREF_URL);
            preferences.Write_String(Constants_Settings.KEY_USR, Constants_Settings.SHARED_PREF_USR);
            preferences.Write_String(Constants_Settings.KEY_PWD, Constants_Settings.SHARED_PREF_PWD);
        }


        mCountDownTimer = new CountDownTimer(500, 500) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                overridePendingTransition(R.animator.animation, 0);
                finish();
            }
        }.start();
    }

    @Override
    public void onBackPressed() {
        mCountDownTimer.cancel();
        super.onBackPressed();
    }

    @Override
    protected void onStop() {
        mCountDownTimer.cancel();
        super.onStop();
    }
}
