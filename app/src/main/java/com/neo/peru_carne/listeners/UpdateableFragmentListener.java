package com.neo.peru_carne.listeners;

/**
 * Created by cesar on 4/11/15.
 */
public interface UpdateableFragmentListener {
    public void onUpdated();
}
