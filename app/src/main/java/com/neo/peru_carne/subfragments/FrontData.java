package com.neo.peru_carne.subfragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.neo.peru_carne.R;
import com.neo.peru_carne.model.CarnePeData;

import org.springframework.util.Base64Utils;

public class FrontData extends android.support.v4.app.Fragment {

    CarnePeData carnePeData;

    public FrontData() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent i = getActivity().getIntent();
        carnePeData = (CarnePeData) i.getSerializableExtra("CarneClass");
        Toast.makeText(getActivity().getApplicationContext(), carnePeData.getStrNombre(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.front_data_ws, null, false);
        ViewHolder viewHolder = new ViewHolder();
        //FrontDataUI
        viewHolder.nombreTxt = (TextView) v.findViewById(R.id.nombreID);
        viewHolder.apePaternoTxt = (TextView) v.findViewById(R.id.apePaternoID);
        viewHolder.apeMaternoTxt = (TextView) v.findViewById(R.id.apeMaternoID);
        viewHolder.dniTxt = (TextView) v.findViewById(R.id.dniID);
        viewHolder.institutoTxt = (TextView) v.findViewById(R.id.institutoID);
        viewHolder.carreraTxt = (TextView) v.findViewById(R.id.carreraID);
        viewHolder.fotoPortador = (ImageView) v.findViewById(R.id.fotoPrincipal);
        v.setTag(viewHolder);

        setUIDatosFrente(carnePeData, viewHolder);
        return v;
    }

    public void setUIDatosFrente(CarnePeData carnePeData, ViewHolder viewHolder) {
        viewHolder.nombreTxt.setText(carnePeData.getStrNombre());
        viewHolder.apePaternoTxt.setText(carnePeData.getStrApePaterno());
        viewHolder.apeMaternoTxt.setText(carnePeData.getStrApePaterno());
        viewHolder.dniTxt.setText(String.valueOf(carnePeData.getIntCodigo()));
        viewHolder.institutoTxt.setText(carnePeData.getStrInstituto());
        viewHolder.carreraTxt.setText(carnePeData.getStrCarrera());

        byte[] bImg = Base64Utils.decode(carnePeData.getImg());
        viewHolder.bitmapFoto = BitmapFactory.decodeByteArray(bImg, 0, bImg.length);
        viewHolder.fotoPortador.setImageBitmap(viewHolder.bitmapFoto);
    }

    static class ViewHolder {
        TextView nombreTxt;
        TextView apePaternoTxt;
        TextView apeMaternoTxt;
        TextView dniTxt;
        TextView institutoTxt;
        TextView carreraTxt;
        ImageView fotoPortador;
        Bitmap bitmapFoto;
    }
}
