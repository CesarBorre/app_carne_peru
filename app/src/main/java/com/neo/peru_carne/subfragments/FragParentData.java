package com.neo.peru_carne.subfragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.neo.peru_carne.R;
import com.neo.peru_carne.adapters.PageAdapterDataWS;


public class FragParentData extends android.support.v4.app.Fragment {
    private ViewPager viewPager;
    private PageAdapterDataWS pagerAdapter;

    private static final int PIN = 0;
    private static final int MAPS = 1;

    private static View selectorPin;
    private static View selectorMaps;
    private static TextView textoPin;
    private static TextView textoMaps;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_frag_parent_data, container, false);

        selectorPin = view.findViewById(R.id.selectorPinID);
        selectorMaps = view.findViewById(R.id.selectorMapsID);
        textoPin = ((TextView) view.findViewById(R.id.title_Pin));
        textoMaps = ((TextView) view.findViewById(R.id.title_Maps));

        selectorPin.setVisibility(View.VISIBLE);
        textoPin.setTextColor(Color.BLACK);
        textoPin.setTypeface(null, Typeface.BOLD);

        pagerAdapter = new PageAdapterDataWS(getChildFragmentManager());

        view.findViewById(R.id.botonPin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(PIN, true);
                pagerAdapter.notifyDataSetChanged();

            }
        });

        view.findViewById(R.id.botonMaps).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(MAPS, true);
                pagerAdapter.notifyDataSetChanged();
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewPager = (ViewPager) getView().findViewById(R.id.viewPagerEvents);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int pageSelected) {
                switch (pageSelected) {
                    case PIN:
                        selectorPin.setVisibility(View.VISIBLE);
                        selectorMaps.setVisibility(View.INVISIBLE);

                        textoPin.setTextColor(Color.BLACK);
                        textoPin.setTypeface(null, Typeface.BOLD);

                        textoMaps.setTextColor(Color.LTGRAY);
                        textoMaps.setTypeface(null, Typeface.NORMAL);

                        pagerAdapter.notifyDataSetChanged();
                        viewPager.setCurrentItem(0);
                        break;

                    case MAPS:
                        selectorMaps.setVisibility(View.VISIBLE);
                        selectorPin.setVisibility(View.INVISIBLE);

                        textoMaps.setTextColor(Color.BLACK);
                        textoMaps.setTypeface(null, Typeface.BOLD);

                        textoPin.setTextColor(Color.LTGRAY);
                        textoPin.setTypeface(null, Typeface.NORMAL);

                        pagerAdapter.notifyDataSetChanged();
                        viewPager.setCurrentItem(1);
                        break;
                }
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
    }


    public class ZoomOutPageTransformer implements ViewPager.PageTransformer {
        private static final float MIN_SCALE = 0.85f;
        private static final float MIN_ALPHA = 0.5f;

        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();
            int pageHeight = view.getHeight();

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0);

            } else if (position <= 1) { // [-1,1]
                // Modify the default slide transition to shrink the page as well
                float scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position));
                float vertMargin = pageHeight * (1 - scaleFactor) / 2;
                float horzMargin = pageWidth * (1 - scaleFactor) / 2;
                if (position < 0) {
                    view.setTranslationX(horzMargin - vertMargin / 2);
                } else {
                    view.setTranslationX(-horzMargin + vertMargin / 2);
                }

                // Scale the page down (between MIN_SCALE and 1)
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);

                // Fade the page relative to its size.
                view.setAlpha(MIN_ALPHA +
                        (scaleFactor - MIN_SCALE) /
                                (1 - MIN_SCALE) * (1 - MIN_ALPHA));

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0);
            }
        }
    }
}
