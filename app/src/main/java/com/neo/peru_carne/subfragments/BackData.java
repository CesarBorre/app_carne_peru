package com.neo.peru_carne.subfragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.neo.peru_carne.R;
import com.neo.peru_carne.model.CarnePeData;

public class BackData extends android.support.v4.app.Fragment {
    CarnePeData carnePeData;
    TextView nombreBackID;


    public BackData() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent i = getActivity().getIntent();
        carnePeData = (CarnePeData) i.getSerializableExtra("CarneClass");
        Toast.makeText(getActivity().getApplicationContext(), carnePeData.getStrNombre(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.back_data_ws, null, false);
        nombreBackID = (TextView) v.findViewById(R.id.nombreBackID);
        nombreBackID.setText(carnePeData.getStrNombre());
        return v;
    }
}
