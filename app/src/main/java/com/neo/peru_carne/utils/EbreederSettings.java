package com.neo.peru_carne.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.neo.peru_carne.R;

import org.w3c.dom.Text;

/**
 * Created by emercado on 9/24/15.
 */
public class EbreederSettings {

    private static Preferences preferences;

    private static SharedPreferences prefs;

    private static final String key ="eBreeder";
    private static final String URL_TAG ="url";
    private static final String USR_TAG ="usr";
    private static final String PSW_TAG ="psw";
    private static final int contextMode= Context.MODE_PRIVATE;
    private static final String defaultUrl="http://ebreeder.neology.com.mx";
    private static final String defaultUsr="raidentrance";
    private static final String defaultPsw="Jokowis123";



    public static SharedPreferences getPrefs() {
        return prefs;
    }

    public static void setPrefs(SharedPreferences prefsIn) {
        prefs = prefsIn;
    }

    public static String getKey() {
        return key;
    }

    public static int getContextMode() {
        return contextMode;
    }
    public static void putUrl(String value){
        getPrefs().edit().putString(URL_TAG,value).apply();
    }

    public static void putUsr(String value){
        getPrefs().edit().putString(USR_TAG,value).apply();
    }

    public static void putPsw(String value){
        getPrefs().edit().putString(PSW_TAG,value).apply();
    }

    public static String getUrl(){
        return getPrefs().getString(URL_TAG,defaultUrl);
    }
    public static String getUsr(){
        return getPrefs().getString(USR_TAG,defaultUsr);
    }
    public static String getPsw(){
        return getPrefs().getString(PSW_TAG,defaultPsw);
    }
    public static void setDefaults(){
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.remove(URL_TAG);
        editor.remove(USR_TAG);
        editor.remove(PSW_TAG);
        editor.apply();
    }

    public static void showSettingsDialog(final Context context, final Context contextClass){

        preferences = new Preferences(Constants_Settings.SHARED_PREF_NAME, contextClass);

        AlertDialog.Builder alertDialogBuilder =  new AlertDialog.Builder(contextClass)
                .setTitle(contextClass.getString(R.string.action_settings))
                .setView(R.layout.settings_dialog)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Dialog dlg = (Dialog) dialog;
                        TextInputLayout textUrl = (TextInputLayout)dlg.findViewById(R.id.textInputAddressID);
                        TextInputLayout textUsr = (TextInputLayout)dlg.findViewById(R.id.textInputAddressID);
                        TextInputLayout textPwd = (TextInputLayout)dlg.findViewById(R.id.textInputAddressID);

                        EditText ip = (EditText) dlg.findViewById(R.id.ipAddress);
                        EditText usr = (EditText) dlg.findViewById(R.id.usr);
                        EditText psw = (EditText) dlg.findViewById(R.id.psw);

                        ip.addTextChangedListener(new MyTextWatcher(ip));
                        usr.addTextChangedListener(new MyTextWatcher(usr));
                        psw.addTextChangedListener(new MyTextWatcher(psw));

                        if (ip != null && ip.getText() != null && !ip.getText().toString().isEmpty() &&
                                usr != null && usr.getText() != null && !usr.getText().toString().isEmpty() &&
                                psw != null && psw.getText() != null && !psw.getText().toString().isEmpty() ) {
                            if(usr.getText().toString().equals("NEOLOGY") &&
                                    psw.getText().toString().equals("NeologyNFC")) {
//                                EbreederSettings.putUrl(ip.getText().toString());
//                                EbreederSettings.putUsr(usr.getText().toString());
//                                EbreederSettings.putPsw(psw.getText().toString() );
                                preferences.Write_String(Constants_Settings.KEY_URL, ip.getText().toString());
                                Toast.makeText(context, contextClass.getString(R.string.settings_set), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, contextClass.getString(R.string.validateUsrPwd), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(context, contextClass.getString(R.string.all_fields_req), Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(R.mipmap.ic_launcher_neo);

        alertDialogBuilder.show();
    }

    static class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.ipAddress:
//                    validateName();
                    break;
                case R.id.usr:
//                    validateEmail();
                    break;
                case R.id.psw:
//                    validatePassword();
                    break;
            }
        }
    }
}
