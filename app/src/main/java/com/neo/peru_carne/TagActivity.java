package com.neo.peru_carne;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.GoogleMap;
import com.neo.peru_carne.adapters.Medical_Contradictions_Adapter;
import com.neo.peru_carne.model.CarnePeData;
import com.neo.peru_carne.model.Medical_Contradictions;
import com.neo.peru_carne.utils.CheckInternetConnection;
import com.neo.peru_carne.utils.Constants_Settings;
import com.neo.peru_carne.utils.EbreederSettings;
import com.neo.peru_carne.utils.Preferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mifareplus.jsonparsing.DocumentStructure;

public class TagActivity extends AppCompatActivity {

    private static final int REQUEST_PLACE_PICKER = 1;

    static final String TAG = TagActivity.class.getSimpleName();
    public static String urlRoute = "";

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private TextView identificationNUmberTxt, dateValidityTxt, emergencyNumberTxt, bloodTypeTxt, placeTxt;
    private TextView medicalContraindications[] = new TextView[6];
    private ImageView moreMedicalContradictionIconID;
    private Button validateBtn;
    private RelativeLayout validateLayOutID;
    private ScrollView scrollVIewTagID;

    private com.getbase.floatingactionbutton.FloatingActionButton settingsBtn, searchBtn, mapBtn;
    private com.getbase.floatingactionbutton.FloatingActionsMenu menuBtnID;

    private android.support.v7.widget.CardView phoneCard, placeCard;

    ProgressDialog progressDialog;

    Preferences preferences;

    private String[] strMedicalContradictions = {
            "DIABETES MELLITUS",
            "BLURRY VISION",
            "EPOC"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = new Preferences(Constants_Settings.SHARED_PREF_NAME, getApplicationContext());
        setContentView(R.layout.activity_tag);
        initElementsUI();
    }

    private void initElementsUI() {
        progressDialog = new ProgressDialog(TagActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Loading data...");

        placeCard = (android.support.v7.widget.CardView) findViewById(R.id.placeCardID);

        placeTxt = (TextView)findViewById(R.id.placeTxtID);

        phoneCard = (android.support.v7.widget.CardView) findViewById(R.id.callPhone);
        phoneCard.setEnabled(false);

        menuBtnID = (FloatingActionsMenu) findViewById(R.id.menuBtnID);
        menuBtnID.setEnabled(false);

        scrollVIewTagID = (ScrollView) findViewById(R.id.scrollVIewTagID);
        scrollVIewTagID.setEnabled(false);

        identificationNUmberTxt = (TextView) findViewById(R.id.identificationNumberTxtID);
        identificationNUmberTxt.setText(DocumentStructure.tagData.get("icn"));

        dateValidityTxt = (TextView) findViewById(R.id.validityDateTxtID);
//        dateValidityTxt.setText(DocumentStructure.tagData.get("name"));
        dateValidityTxt.setText("12-12-2015");

        bloodTypeTxt = (TextView) findViewById(R.id.bloodTypeTxtID);
//        bloodTypeTxt.setText(DocumentStructure.tagData.get("name"));
        bloodTypeTxt.setText("O POSITIVE");

        medicalContraindications[0] = (TextView) findViewById(R.id.medicalContradictionTxtID_1);
//        medicalContraindications[0].setText(DocumentStructure.tagData.get("name"));
        medicalContraindications[0].setText(strMedicalContradictions[0]);

//        medicalContraindications[1] = (TextView) findViewById(R.id.medialContradictionsTxtID_2);
//        medicalContraindications[1].setText(DocumentStructure.tagData.get("name"));
//
//        medicalContraindications[2] = (TextView) findViewById(R.id.medialContradictionsTxtID_3);
//        medicalContraindications[2].setText(DocumentStructure.tagData.get("name"));
//
//        medicalContraindications[3] = (TextView) findViewById(R.id.medialContradictionsTxtID_4);
//        medicalContraindications[3].setText(DocumentStructure.tagData.get("name"));
//
//        medicalContraindications[4] = (TextView) findViewById(R.id.medialContradictionsTxtID_5);
//        medicalContraindications[4].setText(DocumentStructure.tagData.get("name"));
//
//        medicalContraindications[5] = (TextView) findViewById(R.id.medialContradictionsTxtID_6);
//        medicalContraindications[5].setText(DocumentStructure.tagData.get("name"));

        emergencyNumberTxt = (TextView) findViewById(R.id.emergencyPhoneTxtID);
//        emergencyNumberTxt.setText(DocumentStructure.tagData.get("name"));
        emergencyNumberTxt.setText("56581111");
        callNum(emergencyNumberTxt);

        validateBtn = (Button) findViewById(R.id.validateBtnID);
        closeLayout();

        settingsBtn = (FloatingActionButton) findViewById(R.id.settingsBtnID);
        settings();

        searchBtn = (FloatingActionButton) findViewById(R.id.searchBtnID);
        searchDataCarne();

        mapBtn = (FloatingActionButton) findViewById(R.id.mapBtnID);
        loadMap();

        moreMedicalContradictionIconID = (ImageView) findViewById(R.id.moreMedicalContradictionIconID);
        moreMedicalContradictionIconID.setEnabled(false);
        loadMoreMedicalContradictions();
    }

    private void closeLayout() {
        validateLayOutID = (RelativeLayout) findViewById(R.id.validateLayOutID);
        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateLayOutID.setVisibility(View.GONE);
                scrollVIewTagID.setEnabled(true);
                menuBtnID.setEnabled(true);
                phoneCard.setEnabled(true);
                moreMedicalContradictionIconID.setEnabled(true);
            }
        });
    }

    public void callNum(final TextView phone) {
        phoneCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri number = Uri.parse("tel:" + phone.getText().toString());
                Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
                startActivity(callIntent);
            }
        });
    }

    private void settings() {
        settingsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EbreederSettings.showSettingsDialog(getApplicationContext(), TagActivity.this);
            }
        });
    }

    private void searchDataCarne() {
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckInternetConnection.isConnectedToInternet(getApplicationContext())) {
                    callRest();
                } else {
                    Toast.makeText(getApplicationContext(), "NO INTERNET", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void loadMap() {
        mapBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i = new Intent(TagActivity.this, MapsActivity.class);
//                startActivity(i);
                callGooglePlaces();
            }
        });
    }

    private void callRest() {
        progressDialog.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.GET,
                "http://" + preferences.Get_stringfrom_shprf(Constants_Settings.KEY_URL) + "/api/carneAll?intNumeroIdentificacion=4842015000102016",
                null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        readJson(response.toString());
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        // Adding request to request queue
        VolleyApp.getmInstance().addToRequestQueue(jsonObjReq);
    }

    private void readJson(String json) {
        WS ws = new WS();
        ws.execute(json);
    }

    private void loadMoreMedicalContradictions() {
        moreMedicalContradictionIconID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMedicalAlertDialog();
            }
        });
    }

    private void showMedicalAlertDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(TagActivity.this);
        LayoutInflater inflater = getLayoutInflater();

        View convertView = (View) inflater.inflate(R.layout.medical_contradictions_list, null);
        ListView lv = (ListView) convertView.findViewById(R.id.lv);

        List<Medical_Contradictions> medicalList = new ArrayList<Medical_Contradictions>();

        for (int i = 0; i < strMedicalContradictions.length; i++) {
            Medical_Contradictions mC = new Medical_Contradictions();
            mC.setStrMedicalContradiction(strMedicalContradictions[i]);
            mC.setIcon(R.drawable.ic_radio_button_checked);
            medicalList.add(mC);
        }

        Medical_Contradictions_Adapter medicalContradictionsAdapter = new Medical_Contradictions_Adapter(getApplicationContext());
        medicalContradictionsAdapter.addAll(medicalList);
        lv.setAdapter(medicalContradictionsAdapter);

        alertDialog.setView(convertView);
        alertDialog.setTitle(R.string.medicalContradiction);
        alertDialog.show();
    }

    private void callGooglePlaces() {
        try {
            PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();
            Intent intent = intentBuilder.build(getApplicationContext());
            // Start the Intent by requesting a result, identified by a request code.
            startActivityForResult(intent, REQUEST_PLACE_PICKER);

            // Hide the pick option in the UI to prevent users from starting the picker
            // multiple times.
//                    showPickAction(false);

        } catch (GooglePlayServicesRepairableException e) {
            GooglePlayServicesUtil.getErrorDialog(e.getConnectionStatusCode(), TagActivity.this, 0);
        } catch (GooglePlayServicesNotAvailableException e) {
            Toast.makeText(getApplicationContext(), "Google Play Services is not available.",
                    Toast.LENGTH_LONG)
                    .show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // BEGIN_INCLUDE(activity_result)
        if (requestCode == REQUEST_PLACE_PICKER) {
            // This result is from the PlacePicker dialog.

            // Enable the picker option

            if (resultCode == Activity.RESULT_OK) {
                /* User has picked a place, extract data.
                   Data is extracted from the returned intent by retrieving a Place object from
                   the PlacePicker.
                 */
                final Place place = PlacePicker.getPlace(data, getApplicationContext());

                /* A Place object contains details about that place, such as its name, address
                and phone number. Extract the name, address, phone number, place ID and place types.
                 */
                final CharSequence name = place.getName();
                final CharSequence address = place.getAddress();
                final CharSequence phone = place.getPhoneNumber();
                final String placeId = place.getId();
                String attribution = PlacePicker.getAttributions(data);
                if(attribution == null){
                    attribution = "";
                }


                placeCard.setVisibility(View.VISIBLE);
                // Print data to debug log
                Log.d(TAG, "Place selected: " + placeId + " (" + name.toString() + ")");
                placeTxt.setText( "PLACE CHECKED: " +address.toString() );


                // Show the card.

            } else {
                // User has not selected a place, hide the card.
            }

        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
        // END_INCLUDE(activity_result)
    }

    class WS extends AsyncTask<String, Void, Boolean> {
        CarnePeData carnePeData;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                JSONObject object = new JSONObject(params[0]);
                JSONObject carneData = object.getJSONObject("c");

                String intNumeroIdentificacion = carneData.getString("intNumeroIdentificacion");
//            String dtFechaVigenciaIdentificacion = carneData.getString("dtFechaVigenciaIdentificacion");
                String strTipoSangre = carneData.getString("strTipoSangre");
                String strTelefonoEmergencia = carneData.getString("strTelefonoEmergencia");
                String strNombre = carneData.getString("strNombre");
                String strApePaterno = carneData.getString("strApePaterno");
                String strApeMaterno = carneData.getString("strApeMaterno");
//            String strLugarNac = carneData.getString("strLugarNac");
                String strNacionalidad = carneData.getString("strNacionalidad");
                String strInstituto = carneData.getString("strInstituto");
                int intCodigo = carneData.getInt("intCodigo");
                String strCarrera = carneData.getString("strCarrera");
                String strGradoEstudios = carneData.getString("strGradoEstudios");
                double dLat_Ubicacion = carneData.getDouble("dLat_Ubicacion");
                double dLng_Ubicacion = carneData.getDouble("dLng_Ubicacion");
                String strContradiccionMedica_1 = carneData.getString("strContradiccionMedica_1");
                String strContradiccionMedica_2 = carneData.getString("strContradiccionMedica_2");
                String strContradiccionMedica_3 = carneData.getString("strContradiccionMedica_3");
                String strContradiccionMedica_4 = carneData.getString("strContradiccionMedica_4");
                String strContradiccionMedica_5 = carneData.getString("strContradiccionMedica_5");
                String strContradiccionMedica_6 = carneData.getString("strContradiccionMedica_6");
                int id = carneData.getInt("id");
                String img = carneData.getString("imgPortador");
                byte[] bImg = img.getBytes();

                carnePeData = new CarnePeData(
                        intNumeroIdentificacion,
                        strTipoSangre,
                        strTelefonoEmergencia,
                        strNombre,
                        strApePaterno,
                        strApeMaterno,
                        strNacionalidad,
                        strInstituto,
                        intCodigo,
                        strCarrera,
                        strGradoEstudios,
                        dLat_Ubicacion,
                        dLng_Ubicacion,
                        strContradiccionMedica_1,
                        strContradiccionMedica_2,
                        strContradiccionMedica_3,
                        strContradiccionMedica_4,
                        strContradiccionMedica_5,
                        strContradiccionMedica_6,
                        id,
                        bImg);

            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
            if (aVoid) {
                Intent i = new Intent(TagActivity.this, DataActivity.class);
                i.putExtra("CarneClass", carnePeData);
                startActivity(i);
            } else {
                Toast.makeText(getApplicationContext(), "ERROR", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
